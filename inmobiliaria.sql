-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-05-2016 a las 02:39:19
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `inmobiliaria`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarCiudad`(IN `codigo_ciudad1` BIGINT, IN `nombre_ciudad1` VARCHAR(50), IN `fk_estado1` BIGINT)
BEGIN
update Ciudad set nombre_ciudad=nombre_ciudad1, fk_estado=fk_estado1  where codigo_ciudad=codigo_ciudad1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarCliente`(IN `cedula_cliente1` BIGINT, IN `nombre_cliente1` VARCHAR(50), IN `apellido_cliente1` VARCHAR(50), IN `fecha_nacimiento1` DATE, IN `edad_cliente1` BIGINT, IN `genero_cliente1` VARCHAR(50), IN `nombre_aval_referido1` VARCHAR(50), IN `fk_ciudad1` BIGINT, IN `fk_estado1` BIGINT)
BEGIN
update Cliente set nombre_cliente=nombre_cliente1,apellido_cliente=apellido_cliente1,fecha_nacimiento=fecha_nacimiento1,edad_cliente=edad_cliente1, genero_cliente=genero_cliente1,nombre_aval_referido=nombre_aval_referido1,fk_ciudad=fk_ciudad1,fk_estado=fk_estado1 where cedula_cliente=cedula_cliente1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarContrato`(IN `codigo_contrato1` BIGINT, IN `fecha_contrato1` DATE, IN `especificaciones1` VARCHAR(50), IN `valor_estimado1` BIGINT, IN `fk_cliente1` BIGINT, IN `fk_empleado1` BIGINT, IN `fk_tipoContrato1` BIGINT, IN `fk_estado1` BIGINT)
BEGIN
update Contrato set  fecha_contrato=fecha_contrato1,especificaciones=especificaciones1,valor_estimado=valor_estimado1,fk_cliente=fk_cliente1,fk_empleado=fk_empleado1 ,fk_tipoContrato=fk_tipoContrato1,fk_estado=fk_estado1 where codigo_contrato=codigo_contrato1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarDetalleVentaAlquiler`(IN `codigo_detalle_ventaA1` BIGINT, IN `cantidad_inmuebles1` BIGINT, IN `fk_inmueble1` BIGINT, IN `fk_facturaV1` BIGINT)
BEGIN
update Detalle_Venta_Alquiler set cantidad_inmuebles=cantidad_inmuebles1 , fk_inmueble=fk_inmueble1 , fk_facturaV= fk_facturaV1  where codigo_detalle_ventaA=codigo_detalle_ventaA1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarEmpleado`(IN `cedula_empleado1` BIGINT, IN `nombre_empleado1` VARCHAR(50), IN `apellido_empleado1` VARCHAR(50), IN `fecha_nacimiento1` DATE, IN `edad_empleado1` BIGINT, IN `genero_empleado1` VARCHAR(50), IN `contraseña1` VARCHAR(50), IN `fk_rol1` BIGINT, IN `fk_ciudad1` BIGINT, IN `fk_estado1` BIGINT)
BEGIN
update Empleado set nombre_empleado=nombre_empleado1,apellido_empleado=apellido_empleado1,fecha_nacimiento=fecha_nacimiento1,edad_empleado=edad_empleado1, genero_empleado=genero_empleado1,contraseña=contraseña1,fk_rol=fk_rol1,fk_ciudad=fk_ciudad1,fk_estado=fk_estado1 where cedula_empleado=cedula_empleado1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarEstado`(IN `codigo_estado1` BIGINT, IN `nombre_estado1` VARCHAR(50))
BEGIN
Update Estado set nombre_estado=nombre_estado1 where codigo_estado=codigo_estado1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Actualizarfacturaventa`(IN `codigo_facturaV1` BIGINT, IN `fecha_factura1` DATE, IN `total1` BIGINT, IN `fk_contrato1` BIGINT, IN `fk_estado1` BIGINT)
BEGIN
update factura_venta set fecha_factura=fecha_factura1,total=total1,fk_contrato=fk_contrato1,fk_estado=fk_estado1 where codigo_facturaV=codigo_facturaV1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarInmueble`(IN `codigo_inmueble1` BIGINT, IN `direccion_inmueble1` VARCHAR(50), IN `barrio_inmueble1` VARCHAR(50), IN `medida_inmueble1` VARCHAR(50), IN `valor_inmueble1` VARCHAR(50), IN `fk_tipoInmueble1` BIGINT, IN `fk_ciudad1` BIGINT, IN `fk_estado1` BIGINT)
BEGIN
update Inmueble set direccion_inmueble=direccion_inmueble1,barrio_inmueble=barrio_inmueble1, medida_inmueble=medida_inmueble1,valor_inmueble=valor_inmueble1 ,fk_tipoInmueble=fk_tipoInmueble1,fk_ciudad=fk_ciudad1,fk_estado=fk_estado1 where codigo_inmueble=codigo_inmueble1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarInquilino`(IN `cedula_inquilino1` BIGINT, IN `nombre_inquilino1` VARCHAR(50), IN `apellido_inquilino1` VARCHAR(50), IN `fecha_nacimiento1` DATE, IN `edad_inquilino1` BIGINT, IN `genero_inquilino1` VARCHAR(50), IN `fk_inmueble1` BIGINT, IN `fk_estado1` BIGINT)
BEGIN
Update inquilino set nombre_inquilino=nombre_inquilino1,apellido_inquilino=apellido_inquilino1,fecha_nacimiento=fecha_nacimiento1,edad_inquilino=edad_inquilino1, genero_inquilino=genero_inquilino1,fk_inmueble=fk_inmueble1,fk_estado=fk_estado1 where cedula_inquilino=cedula_inquilino1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Actualizarrol`(IN `codigo_rol1` BIGINT, IN `nombre_rol1` VARCHAR(50), IN `fk_estado1` BIGINT)
BEGIN
update rol set nombre_rol=nombre_rol1,fk_estado=fk_estado1 where codigo_rol=codigo_rol1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Actualizartipocontrato`(IN `codigo_tipoContrato1` BIGINT, IN `nombre_tipo_contrato1` VARCHAR(50), IN `fk_estado1` BIGINT)
BEGIN
update tipo_contrato set nombre_tipo_contrato=nombre_tipo_contrato1,fk_estado=fk_estado1 where codigo_tipoContrato=codigo_tipoContrato1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Actualizartipoinmueble`(IN `codigo_tipoInmueble1` BIGINT, IN `nombre_tipo_inmueble1` VARCHAR(50), IN `fk_estado1` BIGINT)
BEGIN
update tipo_inmueble set nombre_tipo_inmueble=nombre_tipo_inmueble1,fk_estado=fk_estado1 where codigo_tipoInmueble=codigo_tipoInmueble1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AddCiudad`(IN codigo_ciudad bigint, IN nombre_ciudad VARCHAR(50), IN fk_estado Bigint)
BEGIN
INSERT INTO Ciudad (codigo_ciudad,nombre_ciudad, fk_estado) VALUES
(codigo_ciudad,nombre_ciudad, fk_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AddCliente`(IN cedula_cliente bigint, IN nombre_cliente VARCHAR(50),IN apellido_cliente VARCHAR(50),IN fecha_nacimiento date,IN edad_cliente bigint,IN genero_cliente varchar(50),IN nombre_aval_referido varchar(50),IN fk_ciudad Bigint,IN fk_estado Bigint)
BEGIN
INSERT INTO Cliente (cedula_cliente,nombre_cliente,apellido_cliente,fecha_nacimiento,edad_cliente, genero_cliente,nombre_aval_referido,fk_ciudad,fk_estado) VALUES
(cedula_cliente,nombre_cliente,apellido_cliente,fecha_nacimiento,edad_cliente, genero_cliente,nombre_aval_referido,fk_ciudad,fk_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AddContrato`(IN codigo_contrato bigint, IN fecha_contrato date,IN especificaciones VARCHAR(50),IN valor_estimado bigint,IN fk_cliente bigint,IN fk_empleado bigint,IN fk_tipoContrato Bigint,IN fk_estado Bigint)
BEGIN
INSERT INTO Contrato (codigo_contrato , fecha_contrato,especificaciones,valor_estimado,fk_cliente,fk_empleado ,fk_tipoContrato,fk_estado) VALUES
(codigo_contrato , fecha_contrato,especificaciones,valor_estimado,fk_cliente,fk_empleado ,fk_tipoContrato,fk_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AddDetalleVentaAlquiler`(IN codigo_detalle_ventaA bigint, IN cantidad_inmuebles bigint,IN fk_inmuebl bigint,IN fk_facturaV bigint)
BEGIN
INSERT INTO Detalle_Venta_Alquiler (codigo_detalle_ventaA,cantidad_inmuebles , fk_inmuebl , fk_facturaV ) VALUES
(codigo_detalle_ventaA,cantidad_inmuebles , fk_inmuebl , fk_facturaV);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AddEmpleado`(IN `cedula_empleado` BIGINT, IN `nombre_empleado` VARCHAR(50), IN `apellido_empleado` VARCHAR(50), IN `fecha_nacimiento` DATE, IN `edad_empleado` BIGINT, IN `genero_empleado` VARCHAR(50), IN `contraseña` VARCHAR(50), IN `fk_rol` BIGINT, IN `fk_ciudad` BIGINT, IN `fk_estado` BIGINT)
BEGIN
INSERT INTO Empleado (cedula_empleado,nombre_empleado,apellido_empleado,fecha_nacimiento,edad_empleado, genero_empleado,contraseña,fk_rol,fk_ciudad,fk_estado) VALUES
(cedula_empleado,nombre_empleado,apellido_empleado,fecha_nacimiento,edad_empleado, genero_empleado,contraseña,fk_rol,fk_ciudad,fk_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AddEstado`(IN codigo_estado bigint, IN nombre_estado VARCHAR(50))
BEGIN
INSERT INTO Estado (codigo_estado,nombre_estado) VALUES
(codigo_estado,nombre_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Addfacturaventa`(IN codigo_facturaV bigint, IN fecha_factura date,IN total bigint, IN fk_contrato bigint, IN fk_estado bigint)
BEGIN
INSERT INTO factura_venta (codigo_facturaV,fecha_factura,total,fk_contrato,fk_estado) VALUES
(codigo_facturaV,fecha_factura,total,fk_contrato,fk_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AddInmueble`(IN codigo_inmueble bigint, IN direccion_inmueble varchar(50),IN barrio_inmueble varchar(50), IN medida_inmueble varchar(50),valor_inmueble varchar(50),IN fk_tipoInmueble bigint,IN fk_ciudad bigint, IN fk_estado bigint)
BEGIN
INSERT INTO Inmueble (codigo_inmueble,direccion_inmueble,barrio_inmueble, medida_inmueble,valor_inmueble ,fk_tipoInmueble,fk_ciudad,fk_estado) VALUES
(codigo_inmueble,direccion_inmueble,barrio_inmueble, medida_inmueble,valor_inmueble ,fk_tipoInmueble,fk_ciudad,fk_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AddInquilino`(IN `cedula_inquilino` BIGINT, IN `nombre_inquilino` VARCHAR(50), IN `apellido_inquilino` VARCHAR(50), IN `fecha_nacimiento` DATE, IN `edad_inquilino` BIGINT, IN `genero_inquilino` VARCHAR(50), IN `fk_inmueble` BIGINT, IN `fk_estado` BIGINT)
BEGIN
INSERT INTO inquilino (cedula_inquilino,nombre_inquilino,apellido_inquilino,fecha_nacimiento,edad_inquilino, genero_inquilino,fk_inmueble,fk_estado) VALUES
(cedula_inquilino,nombre_inquilino,apellido_inquilino,fecha_nacimiento,edad_inquilino, genero_inquilino,fk_inmueble,fk_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Addrol`(IN codigo_rol bigint, IN nombre_rol VARCHAR(50),IN fk_estado Bigint)
BEGIN
INSERT INTO rol (codigo_rol,nombre_rol,fk_estado) VALUES
(codigo_rol,nombre_rol,fk_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Addtipocontrato`(IN codigo_tipoContrato bigint, IN nombre_tipo_contrato VARCHAR(50),IN fk_estado Bigint)
BEGIN
INSERT INTO tipo_contrato (codigo_tipoContrato,nombre_tipo_contrato,fk_estado) VALUES
(codigo_tipoContrato,nombre_tipo_contrato,fk_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Addtipoinmueble`(IN codigo_tipoInmueble bigint, IN nombre_tipo_inmueble VARCHAR(50),IN fk_estado Bigint)
BEGIN
INSERT INTO tipo_inmueble (codigo_tipoInmueble,nombre_tipo_inmueble,fk_estado) VALUES
(codigo_tipoInmueble,nombre_tipo_inmueble,fk_estado);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarCiudad`(in id_ciudad bigint)
select * from ciudad where codigo_ciudad=id_ciudad$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarCliente`(in id_cliente bigint)
select * from cliente where cedula_cliente= id_cliente$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarContrato`(in id_contrato bigint)
select * from Contrato where codigo_contrato=id_contrato$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarDetalleVentaAlquiler`(in id_detalleVentaA bigint)
select * from detalle_venta_alquiler where codigo_detalle_ventaA=id_detalleVentaA$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarEmpleado`(in id_empleado bigint)
select * from empleado where cedula_empleado = id_empleado$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarEstado`(in codigo_estado1 bigint)
BEGIN 
SELECT * FROM estado where codigo_estado = codigo_estado1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarFacturaVenta`(in id_facturaV bigint)
select * from factura_venta where codigo_facturaV= id_facturaV$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarInmueble`(in codigo_inmueble1 bigint)
BEGIN 
select * from Inmueble where codigo_inmueble=codigo_inmueble1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarInquilino`(in cedula_inquilino1 bigint)
BEGIN
SELECT * FROM Inquilino where cedula_inquilino=cedula_inquilino1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarRol`(in id_rol bigint)
select * from rol where codigo_rol =id_rol$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarTipoContrato`(in id_tipoContrato bigint)
select * from tipo_contrato where codigo_tipoContrato = id_tipoContrato$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ConsultarTipoInmueble`(in id_tipoInmueble bigint)
select * from tipo_inmueble where codigo_tipoInmueble = id_tipoInmueble$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarCiudad`(in id_ciudad bigint)
delete from Ciudad  where Codigo_ciudad = id_ciudad$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarCliente`(in id_cliente bigint)
delete from Cliente where cedula_cliente= id_cliente$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarContrato`(in id_contrato bigint)
delete from Contrato where codigo_contrato=id_contrato$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarDetalleVentaAlquiler`(in id_detalleVentaA bigint)
delete from detalle_venta_alquiler where codigo_detalle_ventaA=id_detalleVentaA$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarEmpleado`(in id_empleado bigint)
delete from empleado where cedula_empleado = id_empleado$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarEstado`(in codigo_estado1 bigint)
BEGIN 
DELETE FROM estado where codigo_estado = codigo_estado1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarFacturaVenta`(in id_facturaV bigint)
delete from factura_venta where codigo_facturaV= id_facturaV$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarInmueble`(in codigo_inmueble1 bigint)
BEGIN
DELETE FROM  Inmueble WHERE codigo_inmueble=codigo_inmueble1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarInquilino`(in cedula_inquilino1 bigint)
BEGIN
DELETE FROM Inquilino where cedula_inquilino=cedula_inquilino1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarRol`(in id_rol bigint)
delete from rol where codigo_rol =id_rol$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarTipoContrato`(in id_tipoContrato bigint)
delete from tipo_contrato where codigo_tipoContrato = id_tipoContrato$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarTipoInmueble`(in id_tipoInmueble bigint)
delete from tipo_inmueble where codigo_tipoInmueble = id_tipoInmueble$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE IF NOT EXISTS `ciudad` (
  `codigo_ciudad` bigint(20) NOT NULL,
  `nombre_ciudad` varchar(30) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_ciudad`),
  KEY `fk_estadociudad` (`fk_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`codigo_ciudad`, `nombre_ciudad`, `fk_estado`) VALUES
(1, 'Bogota', 1),
(2, 'Antioquia', 1),
(3, 'Cali', 1),
(4, 'Barranquilla', 2),
(5, 'Cartegena', 2),
(6, 'Cúcuta', 1),
(7, 'Soledad', 1),
(8, 'Ibagué', 1),
(9, 'Bucaramanga', 1),
(10, 'Soacha', 1),
(11, 'Villavicencio', 1),
(12, 'Santa Marta', 1),
(13, 'Pereira', 1),
(14, 'Bello', 1),
(15, 'Valledupar', 1),
(16, 'Monteria', 1),
(17, 'Pasto', 1),
(18, 'Buenaventura', 1),
(19, 'Manizales', 1),
(20, 'Neiva', 1),
(21, 'Palmira', 1),
(22, 'Armenia', 1),
(23, 'Popayan', 1),
(24, 'Sincelejo', 1),
(25, 'Itagui', 1),
(26, 'Riohacha', 1),
(27, 'FloridaBlanca', 1),
(28, 'Envigado', 1),
(29, 'Tuluá', 1),
(30, 'Tumaco', 1),
(31, 'Dosquebradas', 1),
(32, 'Tunja', 1),
(33, 'Barrancabermeja', 1),
(34, 'Girón', 1),
(35, 'Apartadó', 1),
(36, 'Uribia', 1),
(37, 'Florencia', 1),
(38, 'Turbo', 1),
(39, 'Maicao', 1),
(40, 'Piedecuesta', 1),
(41, 'Yopal', 1),
(42, 'Ipiales', 1),
(43, 'Fusagasuga', 1),
(44, 'Cartago', 1),
(45, 'Chia', 1),
(46, 'Pitalito', 1),
(47, 'Zipaquira', 1),
(48, 'Magangué', 1),
(49, 'Malambo', 1),
(50, 'Rionegro', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `cedula_cliente` bigint(20) NOT NULL,
  `nombre_cliente` varchar(30) NOT NULL,
  `apellido_cliente` varchar(30) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `edad_cliente` bigint(20) NOT NULL,
  `genero_cliente` varchar(30) NOT NULL,
  `nombre_aval_referido` varchar(30) NOT NULL,
  `fk_ciudad` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`cedula_cliente`),
  KEY `fk_ciudadcliente` (`fk_ciudad`),
  KEY `fk_estadocliente` (`fk_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`cedula_cliente`, `nombre_cliente`, `apellido_cliente`, `fecha_nacimiento`, `edad_cliente`, `genero_cliente`, `nombre_aval_referido`, `fk_ciudad`, `fk_estado`) VALUES
(10, 'juan', 'wer', '2015-11-29', 15, 'Machote', 'bancomeba', 1, 1),
(1030676001, 'Tobias', 'Castillo', '1996-08-25', 19, 'Masculino', 'BancoCajasocial', 1, 1),
(1030676002, 'Robin', 'Castillo', '1996-08-24', 19, 'Masculino', 'BancoCajasocial', 1, 1),
(1030676003, 'David', 'Castillo', '1996-08-24', 19, 'Masculino', 'BancoCajasocial', 3, 1),
(1030676004, 'Juan', 'Castillo', '1996-08-23', 19, 'Masculino', 'BancoCajasocial', 4, 1),
(1030676005, 'Palita', 'Castillo', '1996-08-23', 19, 'Masculino', 'BancoCajasocial', 5, 1),
(1030676006, 'Paulo', 'Castillo', '1996-08-22', 19, 'Masculino', 'BancoCajasocial', 6, 1),
(1030676007, 'Cristian', 'Castillo', '1996-08-21', 19, 'Masculino', 'BancoAmeba', 7, 1),
(1030676008, 'Juan', 'Castillo', '1996-08-20', 19, 'Masculino', 'BancoAmeba', 8, 1),
(1030676009, 'Rodrigo', 'Castillo', '1996-08-19', 19, 'Masculino', 'BancoAmeba', 9, 1),
(1030676010, 'Juan', 'Castillo', '1996-08-18', 19, 'Masculino', 'BancoAmeba', 10, 1),
(1030676011, 'Casimiro', 'Castillo', '1996-08-17', 19, 'Masculino', 'BancoAmeba', 11, 1),
(1030676012, 'Casimiro', 'Hernandez', '1996-08-16', 19, 'Masculino', 'BancoAmeba', 12, 1),
(1030676013, 'David', 'Hernandez', '1996-08-16', 19, 'Masculino', 'BancoAmeba', 13, 1),
(1030676014, 'Juan', 'Hernandez', '1996-08-17', 19, 'Masculino', 'BancoAmeba', 14, 1),
(1030676015, 'Juanin', 'Hernandez', '1996-08-16', 19, 'Masculino', 'BancoAmeba', 15, 1),
(1030676016, 'Pedro', 'Hernandez', '1996-08-17', 19, 'Masculino', 'BancoAmeba', 16, 1),
(1030676017, 'Manuel', 'Hernandez', '1996-08-16', 19, 'Masculino', 'BancoAmeba', 17, 1),
(1030676018, 'Manuel', 'Hernandez', '1996-08-15', 19, 'Masculino', 'BancoAmeba', 18, 1),
(1030676019, 'Pablo', 'Hernandez', '1996-08-14', 19, 'Masculino', 'BancoAmeba', 19, 1),
(1030676020, 'Fernando', 'Hernandez', '1996-08-13', 19, 'Masculino', 'BancoAmeba', 20, 1),
(1030676021, 'Mario', 'Hernandez', '1996-08-12', 19, 'Masculino', 'BancoAmeba', 21, 1),
(1030676022, 'Berenardo', 'Hernandez', '1996-08-11', 19, 'Masculino', 'BancoAmeba', 22, 1),
(1030676023, 'Paulo', 'Hernandez', '1996-08-11', 19, 'Masculino', 'BancoAmeba', 23, 1),
(1030676024, 'Paulo', 'Martinez', '1996-08-10', 19, 'Masculino', 'BancoAmeba', 24, 1),
(1030676025, 'Pablo', 'Martinez', '1996-08-11', 19, 'Masculino', 'BancoAmeba', 25, 1),
(1030676026, 'Pedro', 'Martinez', '1996-08-10', 19, 'Masculino', 'BancoAmeba', 26, 1),
(1030676027, 'Pacho', 'Martinez', '1996-08-09', 19, 'Masculino', 'BancoAmeba', 27, 1),
(1030676028, 'Pacha', 'Martinez', '1996-08-08', 19, 'Femenino', 'BancoAmeba', 28, 1),
(1030676029, 'Laura', 'Martinez', '1996-08-07', 19, 'Femenino', 'BancoAmeba', 29, 1),
(1030676030, 'Adrea', 'Martinez', '1996-08-06', 19, 'Femenino', 'BancoAmeba', 30, 1),
(1030676031, 'Maria', 'Martinez', '1996-08-05', 19, 'Femenino', 'BancoAmeba', 31, 1),
(1030676032, 'Vanessa', 'Martinez', '1996-08-04', 19, 'Femenino', 'BancoBogota', 32, 1),
(1030676033, 'Paula', 'Martinez', '1996-08-03', 19, 'Femenino', 'BancoBogota', 33, 1),
(1030676034, 'Paula', 'Hernandez', '1996-08-02', 19, 'Femenino', 'BancoBogota', 34, 1),
(1030676035, 'Fabiola', 'Hernandez', '1996-08-02', 19, 'Femenino', 'BancoBogota', 35, 1),
(1030676036, 'Fernanda', 'Hernandez', '1996-08-01', 19, 'Femenino', 'BancoBogota', 36, 1),
(1030676037, 'Paula', 'Mondragon', '1996-07-30', 19, 'Femenino', 'BancoBogota', 37, 1),
(1030676038, 'Fernanda', 'Mondragon', '1996-07-29', 19, 'Femenino', 'BancoBogota', 38, 1),
(1030676039, 'Paula', 'Mondragon', '1996-07-28', 19, 'Femenino', 'BancoBogota', 39, 1),
(1030676040, 'Maria', 'Mondragon', '1996-07-27', 19, 'Femenino', 'BancoBogota', 40, 1),
(1030676041, 'Marta', 'Mondragon', '1996-07-26', 19, 'Femenino', 'BancoBogota', 41, 1),
(1030676042, 'Ruby', 'Mondragon', '1996-07-25', 19, 'Femenino', 'BancoBogota', 42, 1),
(1030676043, 'Teodora', 'Mondragon', '1996-07-25', 19, 'Femenino', 'BancoBogota', 43, 1),
(1030676044, 'Luisa', 'Mondragon', '1996-07-24', 19, 'Femenino', 'BancoBogota', 44, 1),
(1030676045, 'Luisa', 'Roriguez', '1996-07-23', 19, 'Femenino', 'BancoBogota', 45, 1),
(1030676046, 'Andrea', 'Roriguez', '1996-07-22', 19, 'Femenino', 'BanColombia', 46, 1),
(1030676047, 'Cristina', 'Roriguez', '1996-07-21', 19, 'Femenino', 'BanColombia', 47, 1),
(1030676048, 'Maria', 'Roriguez', '1996-07-21', 19, 'Femenino', 'BanColombia', 48, 1),
(1030676049, 'Natalia', 'Roriguez', '1996-07-19', 19, 'Femenino', 'BanColombia', 49, 1),
(1030676050, 'juan', 'Gullermo', '2015-10-26', 18, 'Machote', 'bancomeba', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE IF NOT EXISTS `contrato` (
  `codigo_contrato` bigint(20) NOT NULL,
  `fecha_contrato` date NOT NULL,
  `especificaciones` varchar(50) NOT NULL,
  `valor_estimado` bigint(20) NOT NULL,
  `fk_cliente` bigint(20) NOT NULL,
  `fk_empleado` bigint(20) NOT NULL,
  `fk_tipoContrato` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_contrato`),
  KEY `fk_clientecontrato` (`fk_cliente`),
  KEY `fk_empleadocontrato` (`fk_empleado`),
  KEY `fk_tipocontratocontrato1` (`fk_tipoContrato`),
  KEY `fk_estadocontrato` (`fk_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contrato`
--

INSERT INTO `contrato` (`codigo_contrato`, `fecha_contrato`, `especificaciones`, `valor_estimado`, `fk_cliente`, `fk_empleado`, `fk_tipoContrato`, `fk_estado`) VALUES
(1, '2016-04-17', 'Especificacion001', 1500000, 1030676001, 1030676001, 1, 1),
(2, '2016-04-17', 'Especificacion002', 1500000, 1030676002, 1030676002, 2, 1),
(3, '2016-04-18', 'Especificacion003', 1500000, 1030676003, 1030676003, 3, 1),
(4, '2016-04-18', 'Especificacion004', 1500000, 1030676004, 1030676004, 4, 1),
(5, '2016-04-18', 'Especificacion005', 1500000, 1030676005, 1030676005, 5, 1),
(6, '2016-04-18', 'Especificacion006', 1500000, 1030676006, 1030676006, 6, 1),
(7, '2016-04-18', 'Especificacion007', 1500000, 1030676007, 1030676007, 1, 1),
(8, '2016-04-18', 'Especificacion008', 1500000, 1030676008, 1030676008, 1, 1),
(9, '2016-04-18', 'Especificacion009', 1500000, 1030676009, 1030676009, 1, 1),
(10, '2016-04-18', 'Especificacion010', 1500000, 1030676010, 1030676010, 1, 1),
(11, '2016-04-18', 'Especificacion011', 1500000, 1030676011, 1030676011, 1, 1),
(12, '2016-04-18', 'Especificacion012', 1500000, 1030676012, 1030676012, 1, 1),
(13, '2016-04-18', 'Especificacion013', 1500000, 1030676013, 1030676013, 1, 1),
(14, '2016-04-18', 'Especificacion014', 1500000, 1030676014, 1030676014, 1, 1),
(15, '2016-04-18', 'Especificacion015', 1500000, 1030676015, 1030676015, 1, 1),
(16, '2016-04-18', 'Especificacion016', 1500000, 1030676016, 1030676016, 1, 1),
(17, '2016-04-18', 'Especificacion017', 1500000, 1030676017, 1030676017, 1, 1),
(18, '2016-04-18', 'Especificacion018', 1500000, 1030676018, 1030676018, 1, 1),
(19, '2016-04-18', 'Especificacion019', 1500000, 1030676019, 1030676019, 1, 1),
(20, '2016-04-18', 'Especificacion020', 1500000, 1030676020, 1030676020, 1, 1),
(21, '2016-04-18', 'Especificacion021', 1500000, 1030676021, 1030676021, 1, 1),
(22, '2016-04-18', 'Especificacion022', 1500000, 1030676022, 1030676022, 1, 1),
(23, '2016-04-18', 'Especificacion023', 1500000, 1030676023, 1030676023, 1, 1),
(24, '2016-04-18', 'Especificacion024', 1500000, 1030676024, 1030676024, 1, 1),
(25, '2016-04-18', 'Especificacion025', 1500000, 1030676025, 1030676025, 1, 1),
(26, '2016-04-18', 'Especificacion026', 1500000, 1030676026, 1030676026, 1, 1),
(27, '2016-04-18', 'Especificacion027', 1500000, 1030676027, 1030676027, 1, 1),
(28, '2016-04-18', 'Especificacion028', 1500000, 1030676028, 1030676028, 1, 1),
(29, '2016-04-18', 'Especificacion029', 1500000, 1030676029, 1030676029, 1, 1),
(30, '2016-04-18', 'Especificacion030', 1500000, 1030676030, 1030676030, 1, 1),
(31, '2016-04-18', 'Especificacion031', 1500000, 1030676031, 1030676031, 1, 1),
(32, '2016-04-18', 'Especificacion032', 1500000, 1030676032, 1030676032, 1, 1),
(33, '2016-04-18', 'Especificacion033', 1500000, 1030676033, 1030676033, 1, 1),
(34, '2016-04-18', 'Especificacion034', 1500000, 1030676034, 1030676034, 1, 1),
(35, '2016-04-18', 'Especificacion035', 1500000, 1030676035, 1030676035, 1, 1),
(36, '2016-04-18', 'Especificacion036', 1500000, 1030676036, 1030676036, 1, 1),
(37, '2016-04-18', 'Especificacion037', 1500000, 1030676037, 1030676037, 1, 1),
(38, '2016-04-18', 'Especificacion038', 1500000, 1030676038, 1030676038, 1, 1),
(39, '2016-04-18', 'Especificacion039', 1500000, 1030676039, 1030676039, 1, 1),
(40, '2016-04-18', 'Especificacion040', 1500000, 1030676040, 1030676040, 1, 1),
(41, '2016-04-18', 'Especificacion041', 1500000, 1030676041, 1030676041, 1, 1),
(42, '2016-04-18', 'Especificacion042', 1500000, 1030676042, 1030676042, 1, 1),
(43, '2016-04-18', 'Especificacion043', 1500000, 1030676043, 1030676043, 1, 1),
(44, '2016-04-18', 'Especificacion044', 1500000, 1030676044, 1030676044, 1, 1),
(45, '2016-04-18', 'Especificacion045', 1500000, 1030676045, 1030676045, 1, 1),
(46, '2016-04-18', 'Especificacion046', 1500000, 1030676046, 1030676046, 1, 1),
(47, '2016-04-18', 'Especificacion047', 1500000, 1030676047, 1030676047, 1, 1),
(48, '2016-04-18', 'Especificacion048', 1500000, 1030676048, 1030676048, 1, 1),
(49, '2016-04-18', 'Especificacion049', 1500000, 1030676049, 1030676049, 1, 1),
(50, '2016-04-18', 'Especificacion050', 1500000, 1030676050, 1030676050, 1, 1),
(166, '2016-04-18', 'Especificacion016', 1500000, 1030676016, 1030676016, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta_alquiler`
--

CREATE TABLE IF NOT EXISTS `detalle_venta_alquiler` (
  `codigo_detalle_ventaA` bigint(20) NOT NULL,
  `cantidad_inmuebles` bigint(20) NOT NULL,
  `subtotal` bigint(20) NOT NULL,
  `fk_inmueble` bigint(20) NOT NULL,
  `fk_facturaV` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_detalle_ventaA`),
  KEY `fk_detalleventainmueble` (`fk_inmueble`),
  KEY `fk_delleventafacturaventa` (`fk_facturaV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_venta_alquiler`
--

INSERT INTO `detalle_venta_alquiler` (`codigo_detalle_ventaA`, `cantidad_inmuebles`, `subtotal`, `fk_inmueble`, `fk_facturaV`) VALUES
(1, 2, 2000000, 1, 1),
(2, 2, 2000000, 2, 2),
(3, 3, 2000000, 3, 3),
(4, 4, 2000000, 4, 4),
(5, 5, 2000000, 5, 5),
(6, 6, 2000000, 6, 6),
(7, 1, 2000000, 7, 7),
(8, 1, 2000000, 8, 8),
(9, 1, 2000000, 9, 9),
(10, 1, 2000000, 10, 10),
(11, 1, 1000000, 11, 11),
(12, 1, 1000000, 12, 12),
(13, 1, 1000000, 13, 13),
(14, 1, 1000000, 14, 14),
(15, 1, 1000000, 15, 15),
(16, 1, 1000000, 16, 16),
(17, 1, 1000000, 17, 17),
(18, 1, 1000000, 18, 18),
(19, 1, 1000000, 19, 19),
(20, 1, 1000000, 20, 20),
(21, 1, 3000000, 21, 21),
(22, 1, 3000000, 22, 22),
(23, 1, 3000000, 23, 23),
(24, 1, 3000000, 24, 24),
(25, 1, 3000000, 25, 25),
(26, 1, 3000000, 26, 26),
(27, 1, 3000000, 27, 27),
(28, 1, 3000000, 28, 28),
(29, 1, 3000000, 29, 29),
(30, 1, 3000000, 30, 30),
(31, 1, 3000000, 31, 31),
(32, 1, 3000000, 32, 32),
(33, 1, 3000000, 33, 33),
(34, 1, 3000000, 34, 34),
(35, 1, 3000000, 35, 35),
(36, 1, 3000000, 36, 36),
(37, 1, 3000000, 37, 37),
(38, 1, 3000000, 38, 38),
(39, 1, 3000000, 39, 39),
(40, 1, 4000000, 40, 40),
(41, 1, 4000000, 41, 41),
(42, 1, 4000000, 42, 42),
(43, 1, 4000000, 43, 43),
(44, 1, 4000000, 44, 44),
(45, 1, 4000000, 45, 45),
(46, 1, 4000000, 46, 46),
(47, 1, 4000000, 47, 47),
(48, 1, 4000000, 48, 48),
(49, 1, 4000000, 49, 49),
(50, 1, 4000000, 50, 50);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE IF NOT EXISTS `empleado` (
  `cedula_empleado` bigint(20) NOT NULL,
  `nombre_empleado` varchar(30) NOT NULL,
  `apellido_empleado` varchar(30) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `edad_empleado` bigint(20) NOT NULL,
  `genero_empleado` varchar(30) NOT NULL,
  `contraseña` varchar(30) DEFAULT NULL,
  `fk_rol` bigint(20) NOT NULL,
  `fk_ciudad` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`cedula_empleado`),
  KEY `fk_rolempleado` (`fk_rol`),
  KEY `fk_ciudadempleado` (`fk_ciudad`),
  KEY `fk_estadoemplado` (`fk_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`cedula_empleado`, `nombre_empleado`, `apellido_empleado`, `fecha_nacimiento`, `edad_empleado`, `genero_empleado`, `contraseña`, `fk_rol`, `fk_ciudad`, `fk_estado`) VALUES
(10, 'pepe', 'MALO', '2015-11-30', 24, 'Machote', NULL, 1, 1, 1),
(1030676001, 'Hola', 'Parolas', '2016-05-11', 15, 'Masculino', NULL, 1, 1, 1),
(1030676002, 'Maria', 'Leonora', '1996-10-16', 19, 'Femenino', '1234', 2, 2, 1),
(1030676003, 'Andrea', 'Gonzalez', '1996-10-17', 19, 'Femenino', '1234', 3, 3, 1),
(1030676004, 'Juanita', 'Figueroa', '1996-10-18', 19, 'Femenino', '1234', 4, 4, 1),
(1030676005, 'Juana', 'Hernandez', '1996-10-16', 19, 'Femenino', '1234', 5, 5, 1),
(1030676006, 'Daniela', 'Hernandez', '1996-10-15', 19, 'Femenino', '1234', 6, 6, 1),
(1030676007, 'Vanessa', 'Tamayo', '1996-10-15', 19, 'Femenino', '1234', 7, 7, 1),
(1030676008, 'Elena', 'Hernandez', '1996-10-14', 19, 'Femenino', '1234', 8, 8, 1),
(1030676009, 'Daniela', 'Hernandez', '1996-10-16', 19, 'Femenino', '1234', 9, 9, 1),
(1030676010, 'Mariela', 'Hernandez', '1996-10-13', 19, 'Femenino', '1234', 10, 10, 1),
(1030676011, 'Fernanda', 'Martinez', '1996-09-13', 19, 'Femenino', '1234', 11, 11, 1),
(1030676012, 'Daniela', 'Martinez', '0000-00-00', 19, 'Femenino', '1234', 12, 12, 1),
(1030676013, 'Andrea', 'Martinez', '1996-09-30', 19, 'Femenino', '1234', 13, 13, 1),
(1030676014, 'Vanessa', 'Martinez', '1996-09-29', 19, 'Femenino', '1234', 14, 14, 1),
(1030676015, 'Francisca', 'Martinez', '1996-09-28', 19, 'Femenino', '1234', 15, 15, 1),
(1030676016, 'Elena', 'Martinez', '1996-09-27', 19, 'Femenino', '1234', 16, 16, 1),
(1030676017, 'Fernanda', 'Martinez', '1996-09-26', 19, 'Femenino', '1234', 17, 17, 1),
(1030676018, 'Maria', 'Torres', '1996-09-25', 19, 'Femenino', '1234', 18, 18, 1),
(1030676019, 'Mariela', 'Torres', '1996-09-24', 19, 'Femenino', '1234', 19, 19, 1),
(1030676020, 'Andrea', 'Torres', '1996-09-23', 19, 'Femenino', '1234', 20, 20, 1),
(1030676021, 'Adelle', 'Torres', '1996-09-22', 19, 'Femenino', '1234', 21, 21, 1),
(1030676022, 'Maria', 'Torres', '1996-09-21', 19, 'Femenino', '1234', 22, 22, 1),
(1030676023, 'Tara', 'Torres', '1996-09-20', 19, 'Femenino', '1234', 23, 23, 1),
(1030676024, 'Graciela', 'Torres', '1996-09-19', 19, 'Femenino', '1234', 24, 24, 1),
(1030676025, 'Chabela', 'Hernandez', '1996-09-18', 19, 'Femenino', '1234', 25, 25, 1),
(1030676026, 'Pedro', 'Hernandez', '1996-09-17', 19, 'Masculino', '1234', 26, 26, 1),
(1030676027, 'Tobias', 'Hernandez', '1996-09-16', 19, 'Masculino', '1234', 27, 27, 1),
(1030676028, 'Mario', 'Rodrigo', '1996-09-15', 19, 'Masculino', '1234', 28, 28, 1),
(1030676029, 'Mario', 'Mendez', '1996-09-16', 19, 'Masculino', '1234', 29, 29, 1),
(1030676030, 'Mario', 'Mendoza', '1996-09-15', 19, 'Masculino', '1234', 30, 30, 1),
(1030676031, 'Mario', 'Benedeti', '1996-09-14', 19, 'Masculino', '1234', 31, 31, 1),
(1030676032, 'Juan', 'Torres', '1996-09-13', 19, 'Masculino', '1234', 32, 32, 1),
(1030676033, 'Juanito', 'Torres', '1996-09-12', 19, 'Masculino', '1234', 33, 33, 1),
(1030676034, 'Pepe', 'Torres', '1996-09-12', 19, 'Masculino', '1234', 34, 34, 1),
(1030676035, 'Pablo', 'Torres', '1996-09-09', 19, 'Masculino', '1234', 35, 35, 1),
(1030676036, 'Pablo', 'Mandrago', '1996-09-09', 19, 'Masculino', '1234', 36, 36, 1),
(1030676037, 'Pablo', 'Pedraza', '1996-09-08', 19, 'Masculino', '1234', 37, 37, 1),
(1030676038, 'Patricio', 'Pedraza', '1996-09-07', 19, 'Masculino', '1234', 38, 38, 1),
(1030676039, 'Stivenson', 'Padua', '1996-09-06', 19, 'Masculino', '1234', 39, 39, 1),
(1030676040, 'Timy', 'Padua', '1996-09-04', 19, 'Masculino', '1234', 40, 40, 1),
(1030676041, 'Timy', 'Tapias', '1996-09-03', 19, 'Masculino', '1234', 41, 41, 1),
(1030676042, 'Nelson', 'Tapias', '1996-09-02', 19, 'Masculino', '1234', 42, 42, 1),
(1030676043, 'Camilo', 'Tapias', '1996-09-01', 19, 'Masculino', '1234', 43, 43, 1),
(1030676044, 'Patricio', 'Tapias', '1996-08-30', 19, 'Masculino', '1234', 44, 44, 1),
(1030676045, 'Perencenjo', 'Tapias', '1996-08-30', 19, 'Masculino', '1234', 45, 45, 1),
(1030676046, 'Santiago', 'Tapias', '1996-08-29', 19, 'Masculino', '1234', 46, 46, 1),
(1030676047, 'Santiago', 'Castillo', '1996-08-28', 19, 'Masculino', '1234', 47, 47, 1),
(1030676048, 'Daniel', 'Castillo', '1996-08-27', 19, 'Masculino', '1234', 48, 48, 1),
(1030676049, 'Timio', 'Castillo', '1996-08-26', 19, 'Masculino', '1234', 49, 49, 1),
(1030676050, 'Tobias', 'Castillo', '1996-08-25', 19, 'Masculino', '1234', 50, 50, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `codigo_estado` bigint(20) NOT NULL,
  `nombre_estado` varchar(30) NOT NULL,
  PRIMARY KEY (`codigo_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`codigo_estado`, `nombre_estado`) VALUES
(1, 'sds'),
(2, 'Inactivo'),
(3, 'Hola'),
(6, 'pepito'),
(7, 'activo'),
(8, 'Vendido'),
(15, 'asas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_venta`
--

CREATE TABLE IF NOT EXISTS `factura_venta` (
  `codigo_facturaV` bigint(20) NOT NULL,
  `fecha_factura` date NOT NULL,
  `total` bigint(20) NOT NULL,
  `fk_contrato` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_facturaV`),
  KEY `contratofacturaventa` (`fk_contrato`),
  KEY `fk_estadofacturaventa` (`fk_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura_venta`
--

INSERT INTO `factura_venta` (`codigo_facturaV`, `fecha_factura`, `total`, `fk_contrato`, `fk_estado`) VALUES
(1, '2016-04-17', 1500000, 1, 1),
(2, '2016-04-17', 1500000, 2, 2),
(3, '2016-04-17', 1500000, 3, 1),
(4, '2016-04-17', 1500000, 4, 1),
(5, '2016-04-17', 1500000, 5, 1),
(6, '2016-04-17', 1500000, 6, 1),
(7, '2016-04-17', 1500000, 7, 1),
(8, '2016-04-17', 1500000, 8, 1),
(9, '2016-04-17', 1500000, 9, 1),
(10, '2016-04-17', 1500000, 10, 1),
(11, '2016-04-17', 1500000, 11, 1),
(12, '2016-04-17', 1500000, 12, 1),
(13, '2016-04-17', 1500000, 13, 1),
(14, '2016-04-17', 1500000, 14, 1),
(15, '2016-04-17', 1500000, 15, 1),
(16, '2016-04-17', 1500000, 16, 1),
(17, '2016-04-17', 1800000, 17, 1),
(18, '2016-04-17', 1800000, 18, 1),
(19, '2016-04-17', 1800000, 19, 1),
(20, '2016-04-17', 1800000, 20, 1),
(21, '2016-04-17', 1800000, 21, 1),
(22, '2016-04-17', 1800000, 22, 1),
(23, '2016-04-17', 1800000, 23, 1),
(24, '2016-04-17', 1800000, 24, 1),
(25, '2016-04-17', 1800000, 25, 1),
(26, '2016-04-17', 1800000, 26, 1),
(27, '2016-04-17', 1800000, 27, 1),
(28, '2016-04-17', 1800000, 28, 1),
(29, '2016-04-17', 1800000, 29, 1),
(30, '2016-04-17', 1800000, 30, 1),
(31, '2016-04-17', 1800000, 31, 1),
(32, '2016-04-17', 1900000, 32, 1),
(33, '2016-04-17', 1900000, 33, 1),
(34, '2016-04-17', 1900000, 34, 1),
(35, '2016-04-17', 1900000, 35, 1),
(36, '2016-04-17', 1900000, 36, 1),
(37, '2016-04-17', 1900000, 37, 1),
(38, '2016-04-17', 1900000, 38, 1),
(39, '2016-04-17', 1900000, 39, 1),
(40, '2016-04-17', 1900000, 40, 1),
(41, '2016-04-17', 1900000, 41, 1),
(42, '2016-04-17', 1900000, 42, 1),
(43, '2016-04-17', 1900000, 43, 1),
(44, '2016-04-17', 2000000, 44, 1),
(45, '2016-04-17', 2000000, 45, 1),
(46, '2016-04-17', 2000000, 46, 1),
(47, '2016-04-17', 2000000, 47, 1),
(48, '2016-04-17', 2000000, 48, 1),
(49, '2016-04-17', 2000000, 49, 1),
(50, '2016-04-17', 2000000, 50, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inmueble`
--

CREATE TABLE IF NOT EXISTS `inmueble` (
  `codigo_inmueble` bigint(20) NOT NULL,
  `direccion_inmueble` varchar(30) NOT NULL,
  `barrio_inmueble` varchar(30) NOT NULL,
  `medida_inmueble` varchar(30) NOT NULL,
  `valor_inmueble` varchar(30) NOT NULL,
  `fk_tipoInmueble` bigint(20) NOT NULL,
  `fk_ciudad` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_inmueble`),
  KEY `fk_tipoinmueble1` (`fk_tipoInmueble`),
  KEY `fk_ciudadinmueble1` (`fk_ciudad`),
  KEY `fk_estadoinmueble1` (`fk_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inmueble`
--

INSERT INTO `inmueble` (`codigo_inmueble`, `direccion_inmueble`, `barrio_inmueble`, `medida_inmueble`, `valor_inmueble`, `fk_tipoInmueble`, `fk_ciudad`, `fk_estado`) VALUES
(1, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(2, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(3, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(4, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(5, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(6, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(7, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(8, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(9, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(10, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(11, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(12, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(13, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(14, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(15, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(16, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(17, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(18, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(19, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(20, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(21, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(22, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(23, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(24, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(25, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(26, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(27, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(28, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(29, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(30, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(31, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(32, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(33, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(34, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(35, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(36, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(37, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(38, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(39, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(40, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(41, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(42, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(43, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(44, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(45, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(46, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(47, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(48, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(49, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(50, 'hola', 'juana', '50 cm', '500', 1, 1, 1),
(1554, 'hola', 'we', 'wer', '23', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inquilino`
--

CREATE TABLE IF NOT EXISTS `inquilino` (
  `cedula_inquilino` bigint(20) NOT NULL,
  `nombre_inquilino` varchar(30) NOT NULL,
  `apellido_inquilino` varchar(30) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `edad_inquilino` bigint(20) NOT NULL,
  `genero_inquilino` varchar(30) DEFAULT NULL,
  `fk_inmueble` bigint(20) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`cedula_inquilino`),
  KEY `fk_inmuebleinquilino` (`fk_inmueble`),
  KEY `fk_estadoinquilino` (`fk_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inquilino`
--

INSERT INTO `inquilino` (`cedula_inquilino`, `nombre_inquilino`, `apellido_inquilino`, `fecha_nacimiento`, `edad_inquilino`, `genero_inquilino`, `fk_inmueble`, `fk_estado`) VALUES
(0, 'JUAN', 'MELOZ', '2016-04-14', 15, 'MACHOTE', 1, 1),
(14, 'JUAN', 'Tales', '2014-10-27', 15, 'MACHOTE', 3, 6),
(123, 'weqwe', 'qweqw', '2016-05-03', 123, 'qweq', 19, 2),
(445, 'Jimmy', 'pepe', '2015-11-30', 15, 'MACHOTE', 1, 1),
(1030676001, 'Juan', 'Cebolla', '1997-02-27', 19, 'Masculino', 1, 1),
(1030676002, 'Juanita', 'Garcia', '1997-02-26', 19, 'Femenino', 2, 1),
(1030676003, 'Escobita', 'Barrientos', '1997-02-25', 19, 'Maculino', 3, 1),
(1030676004, 'Elizabeth', 'Manjarres', '1997-02-24', 19, 'femenino', 4, 1),
(1030676005, 'John', 'Limon', '1997-02-23', 19, 'Masculino', 5, 1),
(1030676006, 'Ana', 'Aya', '1997-02-22', 19, 'Femenino', 6, 1),
(1030676007, 'Tomy', 'Takeda', '1997-02-21', 19, 'Masculino', 7, 1),
(1030676008, 'Andrea', 'Murcia', '1997-02-20', 19, 'Femenino', 8, 1),
(1030676009, 'Tomy', 'Yummy', '1997-02-19', 19, 'Masculino', 9, 1),
(1030676010, 'Andrea', 'Arias', '1997-02-18', 19, 'Femenino', 10, 1),
(1030676011, 'Andres', 'Tomates', '1997-01-18', 19, 'Masculino', 11, 1),
(1030676012, 'Andrea', 'Pepe', '1997-01-17', 19, 'Femenino', 12, 1),
(1030676013, 'Andres', 'Pepino', '1997-01-16', 19, 'Maculino', 13, 1),
(1030676014, 'Andrea', 'Arias', '1997-01-17', 19, 'Femenino', 14, 1),
(1030676015, 'Santiago', 'Martinez', '1997-01-16', 19, 'Masculino', 15, 1),
(1030676016, 'Camila', 'Hernandez', '1997-01-16', 19, 'Femenino', 16, 1),
(1030676017, 'Andres', 'Martinez', '1997-01-16', 19, 'Masculino', 17, 1),
(1030676018, 'Fabian', 'Torres', '1997-01-16', 18, 'Masculino', 18, 1),
(1030676019, 'Fabricia', 'Benjamin', '1997-01-15', 18, 'Femenino', 19, 1),
(1030676020, 'Duvan', 'Motavita', '1997-01-05', 19, 'Masculino', 20, 1),
(1030676021, 'Paquita', 'Torres', '1997-01-04', 19, 'Femenino', 21, 1),
(1030676022, 'Pase', 'Canacue', '1997-01-03', 19, 'Masculino', 22, 1),
(1030676023, 'Leidy', 'Padua', '1997-03-03', 19, 'Femenino', 23, 1),
(1030676024, 'Johan', 'Padua', '1997-03-03', 19, 'Masculino', 24, 1),
(1030676025, 'Maria', 'Pimientos', '1997-04-03', 19, 'Femenino', 24, 1),
(1030676026, 'Tobias', 'Torres', '1997-04-03', 19, 'Masculino', 25, 1),
(1030676027, 'Eduard', 'Torres', '1997-04-03', 19, 'Masculino', 27, 1),
(1030676028, 'Paco', 'Malgesto', '1997-01-03', 19, 'Masculino', 28, 1),
(1030676029, 'Jeison', 'Botija', '1997-01-15', 19, 'Masculino', 28, 1),
(1030676030, 'Fabiola', 'Alameda', '1997-01-17', 19, 'Femenino', 28, 1),
(1030676031, 'Michelle', 'Alarcon', '1997-03-17', 19, 'Femenino', 30, 1),
(1030676032, 'Yayita', 'Amaya', '1997-03-17', 19, 'Femenino', 32, 1),
(1030676033, 'Tomasa', 'Figueroa', '1997-02-17', 19, 'Femenino', 33, 1),
(1030676034, 'Clayre', 'Hernandez', '1997-02-18', 19, 'Femenino', 34, 1),
(1030676035, 'Adelle', 'Tapias', '1997-02-19', 19, 'Femenino', 35, 1),
(1030676036, 'Andres', 'Torres', '1997-02-20', 19, 'Masculo', 36, 1),
(1030676037, 'Mario', 'Fernandez', '1997-02-21', 19, 'Masculino', 37, 1),
(1030676038, 'Daniel', 'Tapias', '1997-02-21', 19, 'Masculino', 38, 1),
(1030676039, 'Danilo', 'Figueroa', '1997-02-23', 19, 'Masculino', 39, 1),
(1030676040, 'Danilo', 'Melndez', '1997-02-24', 19, 'Masculino', 40, 1),
(1030676041, 'Danilo', 'Palomino', '1997-02-24', 19, 'Masculino', 41, 1),
(1030676042, 'Sergio', 'Molino', '1997-02-24', 19, 'Masculino', 42, 1),
(1030676043, 'Sergio', 'Mondragon', '1996-12-22', 19, 'Masculino', 43, 1),
(1030676044, 'Pedro', 'Mondragon', '1996-12-22', 19, 'Masculino', 44, 1),
(1030676045, 'Fernando', 'Mondragon', '1996-12-22', 19, 'Masculino', 45, 1),
(1030676046, 'Luigi', 'Mondragon', '1996-12-22', 19, 'Masculino', 46, 1),
(1030676047, 'Timy', 'Guarnizo', '1996-11-22', 19, 'Masculino', 47, 1),
(1030676048, 'Timy', 'Hernandez', '1996-11-26', 19, 'Masculino', 48, 1),
(1030676049, 'Juan', 'Hernandez', '1996-11-16', 19, 'Masculino', 49, 1),
(1030676050, 'juanaa', 'ddada', '2016-12-30', 15, 'MACHOTE', 20, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE IF NOT EXISTS `rol` (
  `codigo_rol` bigint(20) NOT NULL,
  `nombre_rol` varchar(30) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_rol`),
  KEY `fk_estadorol` (`fk_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`codigo_rol`, `nombre_rol`, `fk_estado`) VALUES
(1, 'Administrador', 1),
(2, 'Gerente', 1),
(3, 'Empleado', 1),
(4, 'RecursosHumanos', 1),
(5, 'AsesorComercial', 1),
(6, 'Albañil', 2),
(7, 'Aseador', 2),
(8, 'Tesorero', 1),
(9, 'Lavaplatos', 1),
(10, 'Supervisor', 1),
(11, 'Supervisordeabeja', 1),
(12, 'Digitador', 1),
(13, 'Obrero', 1),
(14, 'Observador', 1),
(15, 'Programador', 1),
(16, 'Organizador', 1),
(17, 'Arquitecto', 1),
(18, 'Contador', 1),
(19, 'Redactor', 1),
(20, 'Analista', 1),
(21, 'Coordinador', 1),
(22, 'Marketing', 1),
(23, 'jefe de Bodega', 1),
(24, 'Subgerente', 1),
(25, 'Abogado', 1),
(26, 'Mantenimiento', 1),
(27, 'Mecanico', 1),
(28, 'Electricista', 1),
(29, 'Evaluador', 1),
(30, 'Vigilante', 1),
(31, 'Seguridad', 1),
(32, 'Supervisor', 1),
(33, 'Superintendente', 1),
(34, 'Motivador', 1),
(35, 'Subcordinador', 1),
(36, 'Desarrollador', 1),
(37, 'Productor', 1),
(38, 'JefeMotivacional', 1),
(39, 'Jefecomercial', 1),
(40, 'Mensajero', 1),
(41, 'Mercaderista', 1),
(42, 'Jefambiental', 1),
(43, 'entretenimiento', 1),
(44, 'Ayudante', 1),
(45, 'Secretaria', 1),
(46, 'Diseñador', 1),
(47, 'Social', 1),
(48, 'Jugador', 1),
(49, 'Jefeinmediato', 1),
(50, 'Elector', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_contrato`
--

CREATE TABLE IF NOT EXISTS `tipo_contrato` (
  `codigo_tipoContrato` bigint(20) NOT NULL,
  `nombre_tipo_contrato` varchar(30) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_tipoContrato`),
  KEY `fk_estadotipocontrato` (`fk_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_contrato`
--

INSERT INTO `tipo_contrato` (`codigo_tipoContrato`, `nombre_tipo_contrato`, `fk_estado`) VALUES
(1, 'Normal', 1),
(2, 'Cuotas', 1),
(3, 'Acuerdo', 1),
(4, 'Cheque', 1),
(5, 'Inmediato', 1),
(6, 'Mes', 1),
(7, 'Año', 1),
(8, 'Intermediario', 1),
(9, 'Internet', 1),
(10, 'Aprendizaje', 1),
(11, 'Desicion', 1),
(12, 'Hipoteca', 1),
(13, 'Cobro', 1),
(14, 'Casa', 1),
(15, 'Venta', 1),
(16, 'Alquiler', 1),
(17, 'Credito', 1),
(18, 'Abono', 1),
(19, 'Cartera', 1),
(20, 'ClienteEmpleado', 1),
(21, 'ClienteGerente', 1),
(22, 'Monetario', 1),
(23, 'Lucrativo', 1),
(24, 'Importante', 1),
(25, 'Noregistra', 2),
(26, 'Analitico', 1),
(27, 'Laboral', 1),
(28, 'Educativo', 1),
(29, 'Promocional', 1),
(30, 'Patrocinio', 1),
(31, 'Ambiental', 1),
(32, 'Primordial', 1),
(33, 'Articulado', 1),
(34, 'Preferencial', 1),
(35, 'Estatal', 1),
(36, 'Industrial', 1),
(37, 'Investigativo', 1),
(38, 'Internacional', 1),
(39, 'Monetario', 1),
(40, 'Politico', 1),
(41, 'Medio', 1),
(42, 'pagoanticipo', 1),
(43, 'Pedagogico', 1),
(44, 'Institucional', 1),
(45, 'Largoplazo', 1),
(46, 'Cortoplazo', 1),
(47, 'Independiente', 1),
(48, 'Policontrato', 1),
(49, 'multicontrato', 1),
(50, 'Cultural', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_inmueble`
--

CREATE TABLE IF NOT EXISTS `tipo_inmueble` (
  `codigo_tipoInmueble` bigint(20) NOT NULL,
  `nombre_tipo_inmueble` varchar(30) NOT NULL,
  `fk_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`codigo_tipoInmueble`),
  KEY `fk_estadotipoinmueble` (`fk_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_inmueble`
--

INSERT INTO `tipo_inmueble` (`codigo_tipoInmueble`, `nombre_tipo_inmueble`, `fk_estado`) VALUES
(1, 'Balcon', 1),
(2, 'Apartamento', 1),
(3, 'Casa', 1),
(4, 'Penhouse', 1),
(5, 'Mansion', 1),
(6, 'Piscina', 1),
(7, 'CasaEmbrujada', 1),
(8, 'Molino', 1),
(9, 'CentroVacacional', 1),
(10, 'Puente', 1),
(11, 'Casa Grande', 1),
(12, 'Casa Pequeña', 1),
(13, 'Casa Mediana', 1),
(14, 'Tres habitaciones', 1),
(15, 'Cuatro habitaciones', 1),
(16, 'Dos habitaciones', 1),
(17, 'Una habitacion', 1),
(18, 'Apartamento Grande', 1),
(19, 'Apartamento Familiar', 1),
(20, 'Apartamento Mediano', 1),
(21, 'Apartamento Pequeño', 1),
(22, 'Casa de campo', 1),
(23, 'Termal', 1),
(24, 'Edificio', 1),
(25, 'Tienda', 1),
(26, 'Carpa', 1),
(27, 'Disco', 1),
(28, 'Fortaleza', 1),
(29, 'Ciudadela', 1),
(30, 'Dos cocinas', 1),
(31, 'Dos Baños', 1),
(32, 'Sala Grande', 1),
(33, 'Sala Mediana', 1),
(34, 'Sala Pequeña', 1),
(35, 'Patio Grande', 1),
(36, 'Patio Pequeño', 1),
(37, 'Patio Mediano', 1),
(38, 'Baticueva', 1),
(39, 'Megacasa', 1),
(40, 'superapartamento', 1),
(41, 'Restaurante', 1),
(42, 'Hotel', 1),
(43, 'Escondite', 1),
(44, 'PlantaBaja', 1),
(45, 'Choza', 1),
(46, 'Sotano', 1),
(47, 'Altillo', 1),
(48, 'Madera', 1),
(49, 'Gris', 1),
(50, 'albergue', 1),
(244, 'Edificio', 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `fk_estadociudad` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`);

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `fk_ciudadcliente` FOREIGN KEY (`fk_ciudad`) REFERENCES `ciudad` (`codigo_ciudad`),
  ADD CONSTRAINT `fk_estadocliente` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`);

--
-- Filtros para la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD CONSTRAINT `fk_clientecontrato` FOREIGN KEY (`fk_cliente`) REFERENCES `cliente` (`cedula_cliente`),
  ADD CONSTRAINT `fk_empleadocontrato` FOREIGN KEY (`fk_empleado`) REFERENCES `empleado` (`cedula_empleado`),
  ADD CONSTRAINT `fk_estadocontrato` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`),
  ADD CONSTRAINT `fk_tipocontratocontrato1` FOREIGN KEY (`fk_tipoContrato`) REFERENCES `tipo_contrato` (`codigo_tipoContrato`);

--
-- Filtros para la tabla `detalle_venta_alquiler`
--
ALTER TABLE `detalle_venta_alquiler`
  ADD CONSTRAINT `fk_delleventafacturaventa` FOREIGN KEY (`fk_facturaV`) REFERENCES `factura_venta` (`codigo_facturaV`),
  ADD CONSTRAINT `fk_detalleventainmueble` FOREIGN KEY (`fk_inmueble`) REFERENCES `inmueble` (`codigo_inmueble`);

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `fk_ciudadempleado` FOREIGN KEY (`fk_ciudad`) REFERENCES `ciudad` (`codigo_ciudad`),
  ADD CONSTRAINT `fk_estadoemplado` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`),
  ADD CONSTRAINT `fk_rolempleado` FOREIGN KEY (`fk_rol`) REFERENCES `rol` (`codigo_rol`);

--
-- Filtros para la tabla `factura_venta`
--
ALTER TABLE `factura_venta`
  ADD CONSTRAINT `contratofacturaventa` FOREIGN KEY (`fk_contrato`) REFERENCES `contrato` (`codigo_contrato`),
  ADD CONSTRAINT `fk_estadofacturaventa` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`);

--
-- Filtros para la tabla `inmueble`
--
ALTER TABLE `inmueble`
  ADD CONSTRAINT `fk_ciudadinmueble1` FOREIGN KEY (`fk_ciudad`) REFERENCES `ciudad` (`codigo_ciudad`),
  ADD CONSTRAINT `fk_estadoinmueble1` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`),
  ADD CONSTRAINT `fk_tipoinmueble1` FOREIGN KEY (`fk_tipoInmueble`) REFERENCES `tipo_inmueble` (`codigo_tipoInmueble`);

--
-- Filtros para la tabla `inquilino`
--
ALTER TABLE `inquilino`
  ADD CONSTRAINT `fk_estadoinquilino` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`),
  ADD CONSTRAINT `fk_inmuebleinquilino` FOREIGN KEY (`fk_inmueble`) REFERENCES `inmueble` (`codigo_inmueble`);

--
-- Filtros para la tabla `rol`
--
ALTER TABLE `rol`
  ADD CONSTRAINT `fk_estadorol` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`);

--
-- Filtros para la tabla `tipo_contrato`
--
ALTER TABLE `tipo_contrato`
  ADD CONSTRAINT `fk_estadotipocontrato` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`);

--
-- Filtros para la tabla `tipo_inmueble`
--
ALTER TABLE `tipo_inmueble`
  ADD CONSTRAINT `fk_estadotipoinmueble` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo_estado`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
