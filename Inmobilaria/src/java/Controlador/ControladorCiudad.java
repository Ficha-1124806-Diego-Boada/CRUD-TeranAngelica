/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Enlace.Conexion;
import Modelo.DatosCiudad;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author REDP
 */
@WebServlet(name = "ControladorCiudad", urlPatterns = {"/ControladorCiudad"})
public class ControladorCiudad extends HttpServlet {
    //Declaro los objetos de la conexion
    Connection cn=null;
    Statement st = null;
    ResultSet rs=null;
    
    //Declaracion de los objetos Modelo y Enlace

    Conexion cnx=new Conexion();
    DatosCiudad Ciudad=new DatosCiudad(); 
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if(request.getParameter("BtnModificar")!=null){
                Ciudad.setId(Integer.parseInt(request.getParameter("txtCodigo")));
                Ciudad.setNombre(request.getParameter("txtNombre"));
                Ciudad.setEstado(Integer.parseInt(request.getParameter("txtEstado")));
                
                if(cnx!=null){
                    st=cnx.getConexion().createStatement();
                    int update=st.executeUpdate("update ciudad set nombre_ciudad='"+Ciudad.getNombre()+"', fk_estado='"+Ciudad.getEstado()+"' where codigo_ciudad='"+Ciudad.getId()+"'");
                    
                    if(update>0)
                    {
                        response.sendRedirect("Ciudad.jsp"); 
                    }
                }
            }
            if (request.getParameter("BtnIngresar")!=null){
                Ciudad.setId(Integer.parseInt(request.getParameter("txtCodigo")));
                Ciudad.setNombre(request.getParameter("txtNombre"));
                Ciudad.setEstado(Integer.parseInt(request.getParameter("txtEstado")));
                
                if(cnx!=null){
                    
                    st=cnx.getConexion().createStatement();
                    int insert=st.executeUpdate("insert into ciudad values ('"+Ciudad.getId()+"','"+Ciudad.getNombre()+"','"+Ciudad.getEstado()+"')");
                    
                    if(insert>0)
                    {
                        response.sendRedirect("Ciudad.jsp");
                    }
                }
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorCiudad.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorCiudad.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
