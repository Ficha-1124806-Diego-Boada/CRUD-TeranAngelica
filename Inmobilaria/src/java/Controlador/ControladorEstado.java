/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Enlace.Conexion;
import Modelo.DatosEstado;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luisa
 */
@WebServlet(name = "ControladorEstado", urlPatterns = {"/ControladorEstado"})
public class ControladorEstado extends HttpServlet {
    //Declaro los objetos de la conexion
    Connection cn=null;
    Statement st = null;
    ResultSet rs=null;
    
    //Declaracion de los objetos Modelo y Enlace

    Conexion cnx=new Conexion();
    DatosEstado Estado=new DatosEstado(); 

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
       
        try {
            if(request.getParameter("BtnModificar")!=null){
            Estado.setId(Integer.parseInt(request.getParameter("txtCodigo")));
            Estado.setNombre(request.getParameter("txtNombre"));
     
            
                if(cnx!=null){

                    st=cnx.getConexion().createStatement();
                    int update=st.executeUpdate("update estado set nombre_estado='"+Estado.getNombre()+"' where codigo_estado='"+Estado.getId()+"'");
                    
                    if(update>0)
                    {
                        response.sendRedirect("Estado.jsp");
                    }
                }
            }
            
            if (request.getParameter("BtnIngresar")!=null){
                              
            Estado.setId(Integer.parseInt(request.getParameter("txtCodigo")));
            Estado.setNombre(request.getParameter("txtNombre"));
            
                if(cnx!=null){
                    
                    st=cnx.getConexion().createStatement();
                    int insert=st.executeUpdate("insert into estado values ('"+Estado.getId()+"','"+Estado.getNombre()+"')");
                    
                    if(insert>0)
                    {
                        response.sendRedirect("Ciudad.jsp");
                    }
                }
            }
            
            
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorEstado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ControladorEstado.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
